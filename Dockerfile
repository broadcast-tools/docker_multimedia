FROM alpine:3.20
LABEL org.opencontainers.image.authors="Stéphane Ludwig <gitlab@stephane-ludwig.net>"

RUN apk add --no-cache --update ffmpeg mediainfo imagemagick
